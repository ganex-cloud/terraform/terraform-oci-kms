output "kms_key_name" {
  value = oci_kms_key.this.display_name
}

output "kms_key_id" {
  value = oci_kms_key.this.id
}
