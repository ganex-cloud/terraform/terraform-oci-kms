variable "compartment_id" {
  description = "(Required) (Updatable) The OCID of the compartment where you want to create resources."
  type        = string
  default     = ""
}

variable "vault_type" {
  description = "(Required) The type of vault to create. Each type of vault stores the key with different degrees of isolation and has different options and pricing."
  type        = string
  default     = ""
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(any)
  default     = null
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = null
}

variable "display_name" {
  description = "(Required) (Updatable) A user-friendly name for the vault. It does not have to be unique, and it is changeable."
  type        = string
  default     = ""
}

variable "management_endpoint" {
  description = "(Required) The service endpoint to perform management operations against."
  type        = string
  default     = ""
}

variable "key_shape" {
  description = "(Required) Master key"
  type = object({
    algorithm = string
    length    = number
  })
}
