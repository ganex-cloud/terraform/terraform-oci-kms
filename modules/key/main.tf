resource "oci_kms_key" "this" {
  compartment_id      = var.compartment_id
  defined_tags        = var.defined_tags
  management_endpoint = var.management_endpoint
  display_name        = var.display_name
  freeform_tags       = var.freeform_tags
  key_shape {
    algorithm = lookup(var.key_shape, "algorithm")
    length    = lookup(var.key_shape, "length")
  }
}
