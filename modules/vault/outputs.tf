output "kms_vault_id" {
  value = oci_kms_vault.this.id
}

output "kms_vault_name" {
  value = oci_kms_vault.this.display_name
}

output "kms_vault_management_endpoint" {
  value = oci_kms_vault.this.management_endpoint
}
